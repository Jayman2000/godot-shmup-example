# Godot Shmup Example
#### Copying
<p xmlns:dct="http://purl.org/dc/terms/" xmlns:vcard="http://www.w3.org/2001/vcard-rdf/3.0#">
  <a rel="license"
     href="http://creativecommons.org/publicdomain/zero/1.0/">
    <img src="http://i.creativecommons.org/p/zero/1.0/88x31.png" style="border-style: none;" alt="CC0" />
  </a>
  <br />
  To the extent possible under law,
  <a rel="dct:publisher"
     href="https://gitlab.com/Jayman2000/godot-shmup-example">
    <span property="dct:title">Jason Yundt</span></a>
  has waived all copyright and related or neighboring rights to
  <span property="dct:title">Godot Shmup Example</span>. See COPYING.txt.
This work is published from:
<span property="vcard:Country" datatype="dct:ISO3166"
      content="US" about="https://gitlab.com/Jayman2000/godot-shmup-example">
  United States</span>.
</p>

Some parts of this game were made by other people or weren't dedicated to the public domain using CC0. Here's a list of projects and what files were derived from them:
  * ["A collection of .gitignore templates"](https://github.com/github/gitignore) by [GitHub](https://github.com/), [CC0 1.0](https://github.com/github/gitignore/blob/master/LICENSE).
    * .gitignore
  * ["Alien UFO pack"](https://kenney.nl/assets/alien-ufo-pack) by [Kenney](https://kenney.nl/), [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/).
    * Art/Player.png
  * ["Animal Pack Redux"](https://kenney.nl/assets/animal-pack-redux) by [Kenney](https://kenney.nl/), [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/).
    * Art/Enemies/Bear.png
    * Art/Enemies/Buffalo.png
    * Art/Enemies/Chicken.png
    * Art/Enemies/Chick.png
    * Art/Enemies/Cow.png
    * Art/Enemies/Crocodile.png
    * Art/Enemies/Dog.png
    * Art/Enemies/Duck.png
    * Art/Enemies/Elephant.png
    * Art/Enemies/Frog.png
    * Art/Enemies/Giraffe.png
    * Art/Enemies/Goat.png
    * Art/Enemies/Gorilla.png
  * CC0 notice from [the CC0 FAQ](https://wiki.creativecommons.org/wiki/CC0_FAQ#May_I_apply_CC0_to_computer_software.3F_If_so.2C_is_there_a_recommended_implementation.3F) by [Diane Peters](https://wiki.creativecommons.org/wiki/User:CCID-diane_peters), [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/).
    * This notice is placed at the top of every souce code file.
  * ["Explosion Sheet"](https://opengameart.org/content/explosion-sheet) by [StumpyStrust](https://opengameart.org/users/stumpystrust), [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/).
    * Art/Explosion.png
  * ["Q009's weapon sounds"](https://opengameart.org/content/q009s-weapon-sounds) by Q009 (submitted by [Calinou](https://opengameart.org/users/calinou)), [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/)
    * Sound Effects\Explosion.ogg
