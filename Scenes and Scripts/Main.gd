# Godot Shmup Example - Example game for people learning Godot
#
# Written in 2020 by Jason Yundt swagfortress@gmail.com
#
# To the extent possible under law, the author(s) have dedicated all copyright
# and related and neighboring rights to this software to the public domain
# worldwide. This software is distributed without any warranty.
#
# You should have received a copy of the CC0 Public Domain Dedication along with
# this software. If not, see
# <http://creativecommons.org/publicdomain/zero/1.0/>.

extends Node

const PLAYER = preload("res://Scenes and Scripts/Player.tscn")

export (float) var scroll_speed = 10 # Unit: pixels per second

func _physics_process(delta):
	$Level.position.x -= scroll_speed * delta

func _ready():
	var to_add = PLAYER.instance()

	to_add.spawn_position = $Level/SpawnPoint.position
	to_add.connect("dead", self, "_on_Player_dead")

	add_child(to_add)
	$Level/SpawnPoint.queue_free()


func _on_Player_dead(lives_left):
	if lives_left < 0:
		set_physics_process(false) # Stop the Level from scrolling

		# How far the screen scrolled. Unit: Pixels
		var final_score = -($Level.position.x)
		$GameOverMessage/Score.text = "Final Score: " + str(round(final_score))
		$GameOverMessage.show()
