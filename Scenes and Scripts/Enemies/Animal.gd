# Godot Shmup Example - Example game for people learning Godot
#
# Written in 2020 by Jason Yundt swagfortress@gmail.com
#
# To the extent possible under law, the author(s) have dedicated all copyright
# and related and neighboring rights to this software to the public domain
# worldwide. This software is distributed without any warranty.
#
# You should have received a copy of the CC0 Public Domain Dedication along with
# this software. If not, see
# <http://creativecommons.org/publicdomain/zero/1.0/>.

tool
extends Node

export (PackedScene) var face = null setget set_face
# NOTE: This variable is a Node, NOT a PackedScene
var current_face = null

func set_face(new_face):
	face = new_face

	# Face off...
	if current_face != null:
		current_face.queue_free()

	# Face on!
	if new_face == null:
		current_face = null
	else:
		current_face = new_face.instance()
		add_child(current_face)
