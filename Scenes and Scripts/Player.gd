# Godot Shmup Example - Example game for people learning Godot
#
# Written in 2020 by Jason Yundt swagfortress@gmail.com
#
# To the extent possible under law, the author(s) have dedicated all copyright
# and related and neighboring rights to this software to the public domain
# worldwide. This software is distributed without any warranty.
#
# You should have received a copy of the CC0 Public Domain Dedication along with
# this software. If not, see
# <http://creativecommons.org/publicdomain/zero/1.0/>.

extends Node2D

const EXPLOSION = preload("res://Scenes and Scripts/Explosion.tscn")
# How fast the user has to move the mouse in order to activate mouse controls.
const MOUSE_CONTROLS_ACTIVATION_SPEED = 50 # Unit: pixels per second

export var max_speed = 309 # Unit: pixels per second
export var lives = 1

# If true, allow the player to move using the mouse. If false, allow the player
# to move using the keyboard.
onready var mouse_controls = false

var WIDTH # Unit: pixels
var HEIGHT # Unit: pixels
var spawn_position = Vector2(0, 0)

signal dead(lives_left)

func _physics_process(delta):
	var new_position
	# The maximum distance the player can travel this tick
	var max_distance = max_speed * delta

	var velocity = Vector2(0, 0)

	if Input.is_action_pressed("ui_left"):
		velocity.x -= Input.get_action_strength("ui_left") # Allow analog input
		mouse_controls = false
	if Input.is_action_pressed("ui_right"):
		velocity.x += Input.get_action_strength("ui_right")
		mouse_controls = false
	if Input.is_action_pressed("ui_up"):
		velocity.y -= Input.get_action_strength("ui_up")
		mouse_controls = false
	if Input.is_action_pressed("ui_down"):
		velocity.y += Input.get_action_strength("ui_down")
		mouse_controls = false

	if mouse_controls:
		# position is relative to the viewport (I think), so we should get the
		# mouse's position relitive to the viewport.
		var mouse_position = get_viewport().get_mouse_position()
		new_position = position.move_toward(mouse_position, max_distance)
	else:
		velocity = velocity.normalized() * max_distance
		new_position = position + velocity

	new_position.x = clamp(new_position.x, 0, WIDTH)
	new_position.y = clamp(new_position.y, 0, HEIGHT)

	position = new_position

func _ready():
	position = spawn_position

	var rect = get_viewport_rect()
	WIDTH = rect.size.x
	HEIGHT = rect.size.y

func _unhandled_input(event):
	if event.is_class("InputEventMouseMotion") \
	and event.relative.length() >= MOUSE_CONTROLS_ACTIVATION_SPEED:
		mouse_controls = true


func _on_Player_area_entered(_area):
	var to_add = EXPLOSION.instance()
	to_add.position = position
	get_parent().add_child(to_add)

	lives -= 1
	emit_signal("dead", lives)

	if lives >= 0:
		position = spawn_position
	else:
		queue_free()
