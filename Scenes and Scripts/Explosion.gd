# Godot Shmup Example - Example game for people learning Godot
#
# Written in 2020 by Jason Yundt swagfortress@gmail.com
#
# To the extent possible under law, the author(s) have dedicated all copyright
# and related and neighboring rights to this software to the public domain
# worldwide. This software is distributed without any warranty.
#
# You should have received a copy of the CC0 Public Domain Dedication along with
# this software. If not, see
# <http://creativecommons.org/publicdomain/zero/1.0/>.

extends Node

onready var animated_sprite_finished = false
onready var audio_stream_player_finished = false

func _ready():
	$AnimatedSprite.play()


func _on_AnimatedSprite_animation_finished():
	animated_sprite_finished = true
	free_if_finished()

func _on_AudioStreamPlayer_finished():
	audio_stream_player_finished = true
	free_if_finished()


func free_if_finished():
	if animated_sprite_finished and audio_stream_player_finished:
		queue_free()
